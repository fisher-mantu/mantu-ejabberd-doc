# Mobile HTTPS API

Mobile API is implemented as an HTTPS endpoint. 
HTTP Request must match common structure:
```
(GET | POST) /2.0/<path>
	...
	Authorization: Token <auth token>
	X-ApiKey: <request encryption data>
	X-Client-Resource: <string>

(json or multipart body)
``` 
where
`<auth token>` is common authentication token,
`<request encryption data>` defines AES key and ivec for request/response body encryption and has following format:
```
base64_encode(rsa_encrypt(base64_encode($aes_256_key) + ":" + base64_encode($aes_256_ivec), $public_key))
``` 
where `public_key` is a key received by client with token;
`X-Client-Resource` is an XMPP resource of client's JID.

If request's `Content-Type` is `multipart/form-data`, each part is a json chat messages or media file content, encrypted with provided key and ivec. For non-multipart requests, body is an arbitrary encrypted json value.

Request's response is an encrypted json value or error of following format:
```json
{"errors": {"code": <integer>}}
```
where error codes can be:

| Code          | Description
|---------------|----------------------------------------------|
| -40000        | Not authenticated (probably invalid token)   |
| -41021        | Account blocked                              |
| -40001        | Token expired                                |
| -41002        | Request is unsupported                       |
| -41000        | Request has invalid format                   |
| -41009        | Not authorized (action is denied)            |
| -41010        | Not found                                    |
| -41015        | Account not found                            |
| -40008        | Host is disabled                             |
| -50000        | Internal error                               |

## Request formats desciption

...
