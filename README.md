# mantu-ejabberd-doc

Documentation effort for Mantu ejabberd team

# 1. Overview

Mantu's jabber server can be split into modified XMPP server (ejabberd)
and mantu-specific features implemented as separate modules in ejabberd
source tree.

## Specific mantu features

* [Groups](feature-groups.md)
* Push notifications (APNS for iOS, google FCM for android)
* File attachments
* Private messages
* P2P messages (describe flow from ejabberd POV)
* Dedicated web-client feature
* [Save-history](feature-save-history.md) feature
* MTOKEN auth

# 2. Dashboard API

Dashboard API is meant to use by the Mantu dashboard only, it utilizes
TCP port 8093 and uses plain HTTP.

For detailed documentation please see [dashboard-api.md](dashboard-api.md)

# 3. Client API

Client API provides messaging middleware with additional services for clients, technically it is:

- port 5222: standard XMPP with mantu extensions
- port 5223: XMPP with direct TLS and optimized connection flow
- port 5290: HTTPS API which is more suitable for some features (file attachments etc)
- port 5292: xmpp-over-websocket for web clients and http poll handler for web client authentication signal
- port 8093: websocket relay for VoIP signaling

## Common definitions for client's API

HTTPS and XMPP API shares same token-base authentication, encryption algorithm and chat message format.
Detailed description: [mobile-api-common.md](./mobile-api-common.md).

## XMPP mantu extensions

- [optimized connection flow](./optimized-xmpp-flow.md)
- [extensions to standard xmpp](./mobile-xmpp-api.md)

## HTTPS API

For detailed description see [mobile-http-api.md](./mobile-http-api.md).

## WebSockets relay

For detailed description see [mobile-ws-api.md](./mobile-ws-api.md).

# 4. Configuration

For configuration files look into `/opt/ejabberd/etc/ejabberd` subdirectory.
Most significant options are in the file `ejabberd.yml` there.

Database configuration can be found by searching keyword `DATABASE
SETUP`. Mantu specific features configured under `mantu_config`
subsection. In order to work correctly, Google's FSM (`fcm`), Apple's APN
service (`ios`) should be configured with correct credentials,
`client_api` should share the same credentials with dashboard,
`ws_relay` should use active ssl certificate, and `save_history`
should have the access to `db` and `sftp` parts of the feature while
sharing the same `psk` with dashboard.

Some modules have its own configuration under `modules` subsection,
most notably `mod_ping` module which specifies the timeout setting to
switch off inactive connections. On bad networks it should be set to
something like 30 secs for `ping_timeout`.

Another notable configuration options are located in `ejabberdctl.cfg`
file in the same directory, also see `/etc/mantu/` subdirectory for
certificates used, and `/etc/sysctl.d/90-ejabberd-sysctl.conf` for
sysctl tweaks used on HA cluster.

# 5. Build/packaging

By default we build two flavours of the server, one installs
system-wide while another installs into `/opt` directory only. The
second case is called `erlang release` and it has a suffix `-rel-`
between package name and package version. All the current deployments
we use as an `erlang release`, the first case is left in case we need
it later.

Building script located in `package/build` in the source
repository. It uses `package/mantu-ejabberd16.spec.template` file to do
the actual build and packaging in RPM file.

This script is used when you build the project using
[jenkins.mantu.im](http://jenkins.mantu.im:8080/) interface, under
`Build-mantu-server-ejabber` project. Click on **Build with
Parameters** on the left-side menu, and choose active git branch to
build from. All the development are going in `devel` branch, all the
stabilized versions are in `master` branch. Feature branches are
merging to `devel`.

The resulting package will have the version based on the latest git
tag from `master` branch followed by the number of commits since the
tag. Branch name goes in the package name if it's not `master` branch
since it is a *flavour* of the software in terms of packaging, so
feature branch `flesh-featule` results in the package name
`mantu-ejabberd16-flesh-featule-rel-0.35.0-231.el7.centos.x86_64.rpm`,
where 0.35.0 is the git tag from the master branch, 231 is the number
of commits since that tag, and `rel` means that this is a `erlang
release` flavour.

# 6. Deployment

Deployment procedure is described in detail in the separate file,
[deployment.md](deployment.md), but in most cases it's enough to login
to [jenkins](http://jenkins.mantu.im:8080), choose `Deploy
mantu-server-ejabber` project, click on **Build with Parameters** in
left-side menu, then choose the ejabberd's version and target server
from the drop-down menues and click **Build** button.

For the latest installations on each server check
[this interface](http://jenkins.mantu.im/deployments), or directly by
local ip, [10.2.1.159](http://10.2.1.159/deployments).
