# Optimized XMPP flows

For iOS we use an optimized XMPP flow, skipping most negotiation steps
up to only one authorization step using XMTOKEN.

The feature replaces standard XMPP stream features negotiation and SASL authentication mechanism. Its purpose is to reduce number of network round-trips required to establish XMPP session, and therefore speed-up connection on slow mobile networks.

Note, that defined protocol doesn't conforms to current XMPP standards and hence will probably require hacks in client XMPP library in order to support it.

## General description

The whole protocol description looks like that:

```xml
C -> S
<stream:stream xmlns:stream="http://etherx.jabber.org/streams" version="1.1" to="{virtual_host}">
<simple-auth xmlns="x:mantu:auth" data="{mantu_token}" resource="{resource}">
</simple-auth>
 
S -> C
 
<?xml version='1.0'?>
 
(<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version="1.1" id='{stream_id}' xml:lang='en'>
    <stream:error>{stream_error_tag}</stream:error>
 </stream:stream> |
    <stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version="1.1" id='{stream_id}' from='{virtual_host}' xml:lang='en'>
    (<simple-auth-success xmlns='x:mantu:auth' jid='{jid}'/> |
        <simple-auth-failure xmlns='x:mantu:auth'>{failure_reason}</simple-auth-failure>
        </stream:stream> )
 
where:
{virtual_host}  - virtual host client is intends to connect
{mantu_token} - valid token, provided during onboarding or token refreshing
{resource} - jid resource for new session
{jid} - authenticated jid
{stream_id} - standard xmpp stream id
{stream_stream_error} - can be <host-unknown> for unknown virtual host
{failure_reason} - possible values are:
    "not-allowed" - authentication denied by ejabberd's ACL, usually means that ejabberd is misconfigured
    "jid-malformed" - may occur when {resource} is too long, for example
    "not-authorized" - invalid token
    "credentials-expired" - expired token
    "blocked" - user was blocked
    "removed" - user was permanently removed from server
```

Note, that initial stream tag, sent by client, contains non-standard stream version, i.e. "1.1" instead of "1.0": with "1.0" version standard XMPP flow will be executed.

With proposed protocol, client doesn't request STARTTLS explicitly, so in order to protect authentication data, server expects from client to establish TLS connection from the start - additional port 5223 will be selected for that.

Zlib compression for stream can be enabled automatically by server from the start of commucation, without any negotiation and signalling. This behaviour is driven by special parameter in server config.
When zlib-from-the-start is enabled, server expects that after establishing tcp connection and tls handshake client immediately upgrade stream to to zlib-compressed state. This means just creating ZLIB compression stream https://tools.ietf.org/html/rfc1950 on top of existing tls socket.

## Server config section example

```yaml
-
  port: 5223
  module: ejabberd_c2s
  protocol_options:
    - "no_sslv2"
    - "no_sslv3"
    - "no_tlsv1"
    - "no_tlsv1_1"
  max_stanza_size: 65536
  shaper: c2s_shaper
  access: c2s
  tls: true
  ciphers: "TLSv1.2:!EXPORT:!LOW:!RC2:!RC4:!MD5:!NULL:!eNULL:!aNULL:!DSS:!aDSS:!TLSv1:!SSLv3:!SSLv2:!AES128:!CAMELLIA128:!IDEA:!SUITEB128:!SUITEB128ONLY"
  tls_required: true
  certfile: "/etc/mantu/server.pem"
  zlib_init: true
```

## Successfull flow

```xml
C -> S
<stream:stream xmlns:stream="http://etherx.jabber.org/streams" version="1.1" to="devlab">
<simple-auth xmlns="x:mantu:auth" data="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIwMDEiLCJob3N0IjoiZGV2bGFiIiwiaWF0IjoxNDg3NzcxMDIwLCJleHAiOjE0ODc4NTc0MjAsImVudiI6MiwiYXR5cGUiOjIsInR0eXBlIjoxfQ.MO2qkBWJ-MzLY2WF0obwSLQYxUpPhuqZMF6JjYZ2Ha0" resource="res">
</simple-auth>
 
S -> C
 
<?xml version='1.0'?>
<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version="1.1" id='3745463736' from='devlab' xml:lang='en'>
<simple-auth-success xmlns='x:mantu:auth' jid='001@devlab/res'/>
```

From this point, connection is successfully authenticated, session created and client can send and receive standard XMPP stanzas, i.e. `<iq>`, `<message>` and `<presence>`. Note, that initial presence or roster request aren't sent automatically.

## Failure flow

```xml
C -> S
<stream:stream xmlns:stream="http://etherx.jabber.org/streams" version="1.1" to="devlab">
<simple-auth xmlns="x:mantu:auth" data="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIwMDEiLCJob3N0IjoiZGV2bGFiIiwiaWF0IjoxNDg3NzcxMDIwLCJleHAiOjE0ODc4NTc0MjAsImVudiI6MiwiYXR5cGUiOjIsInR0eXBlIjoxfQ.MO2qkBWJ-MzLY2WF0obwSLQYxUpPhuqZMF6JjYZ2Ha0" resource="res">
</simple-auth>
 
S -> C
<?xml version='1.0'?>
<stream:stream xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams' version="1.1" id='3745463736' from='devlab' xml:lang='en'> 
<simple-auth-failure xmlns='x:mantu:auth'>not-authorized</simple-auth-failure>
</stream:stream>
// server closes the socket
```
