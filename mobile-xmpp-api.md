# Mobile XMPP API

## Authentication mechanism

Server defines non-standart SASL authentication mechanism called X-MTOKEN.
Client must use it and generate authentication value as follows:

```
$authentication_token + ":" + base64_encode($aes_256_key) + ":" + base64_encode($aes_256_ivec)
```

Possible error conditions:

| Error                   | desription                                      |
| ------------------------|-------------------------------------------------|
| credentials-expired     | token expired                                   |
| temporary-auth-failure  | account and host to which it belongs, blocked   |   
| account-disabled        | account not found, probably was deleted         |
| not-authorized          | token has invalid signature, format or id       |
| malformed-request       | authentication value has invalid format         |

## Application-level encryption

Server decrypts/encrypts in-bound/out-bound XMPP `<message/>` packets with `<body/>` subtag.
In-bound:
```xml
C -> S
<message ...>
	<body>$encrypted_body</body>
</message>

$body = pkcs7_unpad(aes_256_cbc_decrypt(base64_decode($encrypted_body), $aes_256_key, $aes_256_ivec))

S -> router
<message ...>
	<body>$body</body>
</message> 
``` 

Out-bound:
```xml
router -> S
<message ...>
	<body>$body</body>
</message>

$encrypted_body = base64_encode(aes_256_cbc_encrypt(pkc7_pad($body), $aes_256_key, $aes_256_ivec))

S -> C
<message ...>
	<body>$encrypted_body</body>
</message> 
```

## Message acknoledgments

Server responds with special IQ to client's `<message/>` packtes:
```xml
C -> S
<message id="$msg_id">...</message>

S -> C
<iq type="result" ...>
	<r xmlns="x:mantu:delivery">
		<msgid>$msg_id</msgid>
	</r>
</iq>
```

## "Silent" session

Unless client provides following subtag with presense
```xml
<x xmlns='mantu:p2p:silentlogin'><is_silent>false</is_silent></x>
```
session is considered "silent", which means that server won't update "last" value when it will close.

## Clear history request

When client's account has pending "clear history" request, server sends special IQ packet and expect response from client after completion:
```xml
S -> C
<iq type="set" id="$id">
	<clear-history xmlns="mantu:clear-history">
		[ <branch>$branch_id</branch> ]
	</clear-history>

C -> S
<iq type="result" id="$id"/>
```
## Standart services

Standart services of pings and server time and "last online time" is enabled.

## Rosters

Server support standart roster services, except removing roster item.
Also, adding item to roster triggers adding initiator to item's roster and mutual subscription.
For each item in roster server provides information to which branches it is assigned as item's groups.
Client must process standart roster pushes in case of branches set changes and roster items additions.

## VCards

Server allows requesting vcards and updating initiator's vcard image.
Server automatically maintains and injects vcard photo hash value to presence, however hash value depends on _all_ vcard data, not only image.

## Forced stream termination

Server will force client's XMPP stream termination with error `conflict` in case of following events:

| Stream closing tag text description | Reason                                                    |
|-------------------------------------|-----------------------------------------------------------|
| blocked                             | Account was blocked                                       |
| user_removed                        | Account was removed                                       |
| branches_updated                    | Set of branches to which account is assigned was changed  |

Also, in case of account blocking server will change blocked account's presence status from "" to "blocked",
and vice versa in case of unblocking, which will trigger new presence broadcasting to all blocked/unblocked account's subscriptions.
