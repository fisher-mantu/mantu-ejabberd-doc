# Save history feature

Optional feature, can be switched on per company-branch, off by
default.

It saves all the messages in given company-branch pair, including
group messages, file attachments and some special type messages.

By requirement, it uses separate DB in (mostly) write-only mode (it was
desided later that we will use some read operations, too).

DB configuration should be specified in the configuration file, server
and database should be accessible. Mantu server will try to reconnect
to that database on the fly, so it can be considered a
misconfiguration, if this database is not accessible.

Dynamic switching on/off for the branch can be achieved through
dashboard via api (see [dashboard API](dasboard-api.md))

## The workflow

Goes in different threads.

First, per vhost, we're capturing messages through the ejabberd hook,
then parsing it, and storing (but only when storing is ON for that
vhost) for later processing. See `mantu_msg_storage.erl` module.

Second, a special agent starts at the very startup sequence of
ejabberd. This agent is first establish the connection to a database,
and then periodically checks the local storage for messages to
store. If there any, it encrypts its 'text' field and sends to a
remote database, along with other metadata. If operation is
successful, the agent removes stored message from the local
storage. If not, the agent will resend the data later. See
`save_history.erl` module. For file attachments, separate agent is
used, see `mantu_file_history_agent.erl` module.

Third, simple on/off switch is defined in `mantu_save_history.erl`
module, as a cowboy handler to use from dashboard. Also, there is a
special request for `cfg` in order to provide dashboard side with
configuration and credentials for the feature, like database location, 
sftp server location, AES key used to encrypt the messages.

## The configuration

there is a special ejabberd.yml section:

```yaml
ejabberd.yml section
save_history:
  enabled: true
  db_type: mysql
  filestorage_type: sftp
  db:
    host: "remote.mysql.server.com"
    port: 3306
    database: "history"
    user:   "dbuser"
    password: "accesspassword"
    pool_size: 5
  sftp:
    host: "remote.fileserver.com"
    port: 22
    user: "remoteuser"
    pass: "password"
    rmtdir: "upload"
  psk: "bbe71ae72d6635b9cf37eed00a8db493¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦¦"
  spool: "/var/spool/ejabberd"
  period: 2
```

1. If **enabled: false**, no messages will be stored at all.
2. **psk** should be exacltly 48 bytes long, 32 bytes Key + 16 bytes IV
3. **period** specifies the time period in seconds to run saving agent
4. **db_type** either pgsql or mysql (dashboard though can use only
   mysql)
5. **filestorage_type** for now only sftp is supported (the only
   method supported by dashboard is sftp)
6. **spool** is used as temp storage before actual storage on remote
7. **pool_size** is the number of db connections to history database

## The schema

The schema for DB is defined in priv/sql/save_history.sql file.

```sql
-- main history message table
CREATE TABLE message
(
  src_id       varchar(50)     NOT NULL,
  src_fn       varchar(191),
  src_ln       varchar(191),
  dst_id       varchar(191)    NOT NULL,
  dst_fn       varchar(191),
  dst_ln       varchar(191),
  vhost        VARCHAR(64)     NOT NULL,
  chat_type    SMALLINT        NOT NULL,
  privacy_mode BOOLEAN         NOT NULL,
  msg_type     SMALLINT        NOT NULL,
  msg_id       VARCHAR(64)     NOT NULL,
  branch       bigint          NOT NULL,
  ts           TIMESTAMP(3)    NOT NULL,
  json         text,
  id           BIGINT UNSIGNED NOT NULL AUTO_INCREMENT UNIQUE PRIMARY KEY
);

-- index msg 1
CREATE INDEX message_vhost_from_target_chat_type_ts_idx
  ON message (vhost, src_id, dst_id, chat_type);

-- attachments
create table attach
(
  file_id   varchar(191) NOT NULL primary key,
  msg_id    varchar(191),
  original_name varchar(191),
  original_size bigint,
  original_type bigint,
  storage   enum ('file', 'sftp', 's3'),
  rmt_path  varchar(191),
  rmt_file  varchar(24),
  rmt_size  bigint
);
```

## the API

using basic auth for dashboard:


### [GET] /api/dashboard/save_history/[VHOST]/on/[BRANCH]

start the writing of the messages for a given vhost/branch. By
default, any writing is off until it will be switched on on dashboard.

in: VHOST company hash

in: BRANCH numeric id

return: 200, `{ok, on}` || `{error, Reason\}`


### [GET] /api/dashboard/save_history/[VHOST]/off/[BRANCH]

stop storing messages for a given vhost/branch

in: VHOST company hash

in: BRANCH numeric id

return: 200, `{ok, off}` || `{ok, no_such_vhost}` || `{error, Reason}`


### [GET] /api/dashboard/save_history/cfg/[SALT]

get the configuration to access remote servers

in: SALT – random 16 bytes long string encoded in base64url

return: 200, `{ok, Blob}` || `{error, Reason}`

`Blob` – encoded in base64url, encrypted with AES CBC mode, json object
with configuration.

(Using PSK defined in ejabberd.yml and given `SALT` as IV)

example json after decryption
```javascript
{"sftp":
    {"user":"filestorage", "rmtdir":"upload", "port":2200,
     "pass":"asdfpoiuqwer12", "host":"svartalfheimr.heim.kiev.ua"},
"psk":"lN1465x0sshMH1H2z23zOls-5M45Y6_58IG4TN-cy2k",
"iv":"RSKIrSzFle1wXn30ci1YXg",
"db":
    {"user":"postgres", "port":5432, "password":"Xmpp12Xmpp12",
     "host":"127.0.0.1", "database":"history"}
}
```

## the CLI interface

For convenience, there is a command provided to use with `ejabberdctl`
tool. It shows the status of the feature:

```shell
# ejabberdctl save-history-status
h26c7d048068bd9f749cf57261f8fe83b       9, 1
ha47f3ca4b709a8bb004bcccc4b05c2ac       12
h76e15760a8842dfb1f8f687cb62bb66d       64
```

...shows that the feature is enabled for `vhost` provided in first field
and comma-separated list of branch IDs within that company.

## the undone

* it is not designed to use multiple AES keys with stored messages --
  no such task were given at planning stage;
* there is no support for other methods to store files except sftp;
* need to check if we need the file first. Right now it is first
  stored and then checked if we need it to store in attachment;
* the feature was rewritten twice which reflects its current state
  with unused artifacts in the code
