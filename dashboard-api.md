# Dashboard API

For dashboard api we use cowboy listening on port 8092. The routes
are specified in `mantu_internal_http` module.

API should be considered as internal, used only by the dashboard.

Basic authorization, no SSL.

## "/api/healthcheck"

Call to `mantu:init(Req, healthcheck)`. Used to check the server's availability
for cluster setup.

## "/api/dashboard/virtual_host/[...]"

Call to `mantu_virtual_host:init(Req, [])`. Used to
create/modify/delete virtual host AKA company.

### Method GET

Used to get the list of available vhosts or a given _vhost_.

Returns JSON object:

```javascript
{
   "payload":[
      {
         "odbc_database":"h26c7d048068bd9f749cf57261f8fe83b",
         "enabled":true,
         "dn":"h26c7d048068bd9f749cf57261f8fe83b"
      },
      {
         "odbc_database":"h4322543cbf77c3ef2ea8f50abfb3d714",
         "enabled":false,
         "dn":"h4322543cbf77c3ef2ea8f50abfb3d714"
      },
      {
         "odbc_database":"h57cdc01565fd58da06bc728b327d33e4",
         "enabled":true,
         "dn":"h57cdc01565fd58da06bc728b327d33e4"
      },
      {
         "odbc_database":"h6b2939d0283d502130fe7ff8926eece2",
         "enabled":true,
         "dn":"h6b2939d0283d502130fe7ff8926eece2"
      },
      {
         "odbc_database":"h71a92764501be7dd7c9e5317e007b603",
         "enabled":true,
         "dn":"h71a92764501be7dd7c9e5317e007b603"
      },
      {
         "odbc_database":"h9ac8d58147a1f59c6b57f5fa79cce1fe",
         "enabled":true,
         "dn":"h9ac8d58147a1f59c6b57f5fa79cce1fe"
      },
      {
         "odbc_database":"ha6b01ed9ff1eb711c34853ff101fbacc",
         "enabled":false,
         "dn":"ha6b01ed9ff1eb711c34853ff101fbacc"
      }
   ]
}
```

### Method PUT

Used to create/update given _vhost_. Handler expects json
object on input,

```javascript
{"odbc_database": _DB_name_, "enabled": _boolean_}
```

Returns 200 with object

```javascript
{"payload":0}
```

or error if something went wrong.

### Method DELETE

Used to delete given _vhost_.

Should return 200 if everything's ok.

Example:

```shell
SERVER="dev-jab.mantu.im"
VHOST="asdfqwer"
curl -4v -u mantu-devlab:g20AAAAKMzAwNzI2MzI1Ng== -X DELETE http://${SERVER}:8092/api/dashboard/virtual_host/$VHOST
```

Returns:
```
*   Trying 34.240.72.177...
* TCP_NODELAY set
* Connected to dev-jab.mantu.im (34.240.72.177) port 8092 (#0)
* Server auth using Basic with user 'mantu-devlab'
> DELETE /api/dashboard/virtual_host/qwerasdf HTTP/1.1
> Host: dev-jab.mantu.im:8092
> Authorization: Basic bWFudHUtZGV2bGFiOmcyMEFBQUFLTXpBd056STJNekkxTmc9PQ==
> User-Agent: curl/7.52.1
> Accept: */*
> 
< HTTP/1.1 200 OK
< content-length: 13
< content-type: application/json
< date: Thu, 09 Aug 2018 06:36:53 GMT
< server: Cowboy
< 
* Curl_http_done: called premature == 0
* Connection #0 to host dev-jab.mantu.im left intact
{"payload":0}
```

## "/api/dashboard/subscriber/block/:vhost/"

Call to `mantu_rest_subscriber_opt:init(Req, [block])`

Used to block a subscriber from using the service.

Expects JSON object on PUT/POST:

```javascript
[ {"msisdn": _Subscriber_, "is_blocked": _boolean_} ]
```

## "/api/dashboard/subscriber/branch/:vhost/[:msisdn]"

Call to `mantu_rest_subscriber_opt:init(Req, [branch])`

### Method PUT/POST

Used to assign a branch(es) to a subscriber(s)

Expects JSON object on input:

```javascript
[ {"msisdn": _Subscriber_, 
   "add_branches": [ _Branch_id_ ], "remove_branches": [ _Branch_id_ ] ]
```

### Method GET

Used to list subscribers on a given _branch_ of a given _vhost_.

## "/api/dashboard/subscriber/import/:vhost/"

Call to `mantu_rest_subscriber_opt:init(Req, [import])`

Used for mass creation of subscribers for a given _vhost_.

JSON object is as follows:

```javascript
{"subscribers":
    [ {"vcard": {"firstname": _FirstName_, "lastname": _LastName_ },
       "msisdn": _Subscriber_ } ]
}
```

## "/api/dashboard/subscriber/clear-history/:vhost/[:branch]"

Call to `mantu_rest_subscriber_opt:init(Req, [history])`

Used to send *clear_history* request to _subscriber_ of a given _vhost_.

```javascript
{"msisdns": [ _Msisdn_ ]}
```

## "/api/dashboard/subscriber/invitation-available/:vhost/"

Call to `mantu_rest_subscriber_opt:init(Req, [invitation])`

```javascript
{"msisdns": [ _Msisdn_ ]}
```

## "/api/dashboard/subscriber/:vhost/[:msisdn]"

Call to `mantu_rest_subscriber:init(Req, [])`

### Method GET

Used to list subscribers for a given _vhost_.

Optional query parameters for method GET:

* *count* = _boolean_.
   Show the number of users, don't list them all
* *from* = _integer_.
   Used in sql pagination. Should be supplemented with *to* parameter.
* *to* = _integer_.
   Used in sql pagination.
* *branch* = _integer_.
   Use a given branch in request.
* *search* = _string_.
   Search for a given _msisdn_ or _firstname_ or _lastname_
* *sort* = _string_.
   Sort by given comma-separated fields in _string_.

### Method POST

Used to *create* a single subscriber.

Expected JSON object:

```javascript
{ "msisdn": _Msisdn_,
  "vcard": { "firstname": _FirstName_, "lastname": _LastName_ }
}
```

### Method PUT

Used to *update* a single subscriber.

Expected JSON object:

```javascript
{ "msisdn": _Msisdn_,
  "vcard": { "firstname": _FirstName_, "lastname": _LastName_ }
}
```

### Method DELETE

Used to delete a subscriber.

Nota bene: this method expects JSON on input,

```javascript
{"msisdn": _Msisdn_}
```

Where _Msisdn_ can be a single subscriber, a json list of subscribers,
or a comma-separated list of subscribers.

## "/api/dashboard/virtual-branch/:vhost/[:branch]"

Call to `mantu_rest_branch:init(Req, [])`

### Method GET

Optional query parameters for method GET:

1. *branch* = _integer_
   If set, look for only this branch, supplement it with name and status
2. *enable* = _boolean_
   list only enabled branches
3. *from* = _integer_
   used for sql pagination
4. *to* = _integer_
   used for sql pagination
5. *name* = _string_
   look for the branch name

Returns

```javascript
{"payload": RecordList, "error": [], "count": ListCounter}
```

### Method POST

Used to update branch status or name

Expects

```javascript
{"enable": _boolean_, "name": _string_}
```

Where _name_ is optional branch name.

### Method DELETE

Used to delete branch with given _branch_id_.

## "/api/dashboard/group/subscriber/:vhost/:branch/[:groupid]"

Call to `mantu_rest_group_subscriber:init(Req, [])`

### Method GET

Used to list group members for a given _groupid_.

Mandatory query parameters:

1. *from* = _integer_
2. *to* = _integer_
   Used for sql pagination

#### Example

```shell
VHOST="h26c7d048068bd9f749cf57261f8fe83b"
BID="1"
GID="5cb3b20a-5eb5-42d4-9489-b0e6e5421ad5"
curl -4 -u mantu-devlab:g20AAAAKMzAwNzI2MzI1Ng== -X GET "http://${SERVER}:8092/api/dashboard/group/subscriber/${VHOST}/${BID}/${GID}?from=1&to=100"
```

```javascript
{
   "payload":[
      {
         "members":[
            {
               "vcard":{
                  "role":"bot",
                  "lastname":"Monokorameter",
                  "firstname":"Heterocekehuible",
                  "email":"devnull@heim.in.ua",
                  "department":"automated world dominance"
               },
               "msisdn":"+000917854841"
            },
            {
               "vcard":{
                  "role":"",
                  "lastname":"Huiway",
                  "firstname":"Nexuis",
                  "email":"devnull@heim.in.ua",
                  "department":""
               },
               "msisdn":"+380730479562"
            }
         ],
         "id":"5cb3b20a-5eb5-42d4-9489-b0e6e5421ad5"
      }
   ],
   "count":2
}
```

### Method POST

Used to add subscribers listed in _msisdns_ to a group(s).

```javascript
{"msisdns": [ _Msisdn_ ], "ids": [ _GroupID_ ] }
```

### Method DELETE

Used to delete subscribers from group(s)

```javascript
{"msisdns": [ _Msisdn_ ], "ids": [ _GroupID_ ] }
```

## "/api/dashboard/group/subscriber-available/:vhost/:branch/:groupid"

Call to `mantu_rest_group_subscriber:init(Req, availiable)`

### Method GET

Used to get available subscribers (i.e. subscribers available to add)
for a given group id.

Mandatory query parameters are *from* and *to*, used in sql
pagination.

Returns
```javascript
{"payload": [ {"members": [_Member_], "id": _GroupID_} ],
 "count": _MemberCounter_
}
```

Example:

```shell
VHOST="h26c7d048068bd9f749cf57261f8fe83b"
BID="1"
GID="5cb3b20a-5eb5-42d4-9489-b0e6e5421ad5"
curl -4 -u mantu-devlab:g20AAAAKMzAwNzI2MzI1Ng== -X GET "http://${SERVER}:8092/api/dashboard/group/subscriber-available/${VHOST}/${BID}/${GID}?from=1&to=100"
```

```javascript
{
   "payload":[
      {
         "members":[
            {
               "vcard":{
                  "role":"bot",
                  "lastname":"Duofaty",
                  "firstname":"Incohapaetichrom",
                  "email":"asdf@dev.null",
                  "department":"automated world dominance"
               },
               "msisdn":"+000116452673",
               "is_blocked":false
            },
            {
               "vcard":{
                  "role":"",
                  "lastname":"6+",
                  "firstname":"iPhone",
                  "email":"mantuiphone6@icloud.com",
                  "department":""
               },
               "msisdn":"+972550184263",
               "is_blocked":false
            }
         ],
         "id":"5cb3b20a-5eb5-42d4-9489-b0e6e5421ad5"
      }
   ],
   "count":2
}
```

## "/api/dashboard/group/:vhost/:branch/[:groupid]"

Call to `mantu_rest_group:init(Req, [])`

Used to add/update/delete group(s).

## "/api/dashboard/token/[...]"

Call to `mantu_rest_token:init(Req, [])`

Used to issue/reissue Mantu token.

Returns HTTP 200 on success with token in JSON object or HTTP 400 on
error with error description in JSON object.

Example:

```shell
SERVER="dev-jab.mantu.im"
VHOST="h26c7d048068bd9f749cf57261f8fe83b"
PHONE="+380919041600"
curl -4v -u mantu-devlab:g20AAAAKMzAwNzI2MzI1Ng== -H "Content-Type: application/json" -X PUT http://$SERVER:8092/api/dashboard/token/issue -d '{"msisdn": "'$PHONE'", "host": "'$VHOST'"}'
```

Returns

```javascript
{"t":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJ2IjoxMiwidWlkIjoiKzM4MDkxOTA0MTYwMCIsImp0aSI6InEtbTNwczcwMGw4IiwiaWF0IjoxNTMzNzk3MDM2LCJob3N0IjoiaDI2YzdkMDQ4MDY4YmQ5Zjc0OWNmNTcyNjFmOGZlODNiIiwiZXhwIjoxNTQ5MzQ5MDM2fQ.4sbvCDMu9FouR1RPvPDnaqio_lNXBVZfpLCeY9GyuErWCDz6EfOWkCjqWrcL_l1m_rXg88ov2OlM8ppUlNgTjg","pub_key":"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtX39sUv55pSKD6L8zHIHYEUiZjR75T17I6WSyiQXYFHELumNkZ96hf5jEOCHAwmvK1PXxmw5aX3tGj66EPhcuPwpuGpGg/VnWuWU7eF0Jd56MRw6Le+SPkSefdUy3gysV0TDwq8LJX2HUApipRWmQdATfU4bwlHhD8GYkBZPLU11P69jfaHKxkdXCWSlhCz+SfFIpHDJ2j7pJFpjqUnEAbqzsvrjmVlW/7kdEhq6vIv8t8CHBK3P83Hgd9raUMdPsBjg494MH/Ho05Xf/330/on0+OVd8M3Qn3Glzce0mBVoauO2mkdLgdb7DbfhSXFYSS+HaD1DgS/8k61zl+SWQwIDAQAB"}
```

## "/api/dashboard/save_history/[...]"

Call to `mantu_save_history:init(Req, [])`

Used to add/remove pair {vhost, branchid} to
[save-history](feature-save-history.md) feature, and also to provide
the dashboard with feature configuration.


## "/api/dashboard/qrcode/[...]"

Call to `mantu_qrcode:init(Req, [])`

Used to work with QR codes for webclient workflow.
