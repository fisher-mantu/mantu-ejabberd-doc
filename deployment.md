# Deployment

This document describes manual deployment procedure.

# Silent assumptions

* the installation process should be separated from the building process
* installation should be as easy as yum install package
* setup tricks process should be written down here

# 0. install the new VPS with centos 7.x installed

Either on AWS or locally through QEMU

# 1. ensure EPEL repository is installed

```shell
$ sudo yum install epel-release
```

# 2. ensure Erlang Solutions repository is installed

```shell
$ sudo yum install wget
$ wget https://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
$ sudo rpm -Uvh erlang-solutions-1.0-1.noarch.rpm
```

# 3. ensure Mantu CentOS 7 repository is installed

This step is optional, do it if you want to install packages via
convenient Centos package management system, **yum**.

```shell
$ wget http://10.2.1.159/centos/mantu-centos7.repo
$ sudo mv mantu-centos7.repo /etc/yum.repos.d/
```

Nota bene: Just for now I use repository at jenkins.mantu.im server, but
it seems to be inaccessible from the new AWS VPSes. Maybe we need to
relocate repository somewhere else?

search for available packages through the YUM package management system
it should be available at this point:

(from the target machine)

```shell
[fisher@centos7 ~]$ yum search ejabberd16
[...]
mantu-ejabberd16.x86_64 : Signaling server for mantu.im, based on ejabberd
mantu-ejabberd16-rel.x86_64 : Signaling server for mantu.im, based on ejabberd

[fisher@centos7 ~]$ yum info mantu-ejabberd16.x86_64
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.mirohost.net
 * epel: ftp.colocall.net
 * extras: mirror.mirohost.net
 * updates: mirror.mirohost.net
Installed Packages
Name        : mantu-ejabberd16
Arch        : x86_64
Version     : 0.27.0
Release     : 15.el7.centos
Size        : 6.6 M
Repo        : installed
Summary     : Signaling server for mantu.im, based on ejabberd
URL         : http://www.mantu.im
License     : Copyright ? 2016 Mantu Mobile LTD.
Description : Signaling server for mantu.im secure messenger
            : derived from ejabberd open-source XMPP implementation.
            : It speaks XMPP and HTTP/S. Ejabberd architechture
            : preserved, new features implemented on top of it.
```

if there is no mantu-ejabberd package available, it means there's
something wrong with yum repository configuration.

_Hint_ – if you just rebuilt a new rpm package and have added it to
the /repos/centos/7/Package directory, don't forget to

* run as root /repos/centos/update-repos.sh on building machine. This
  will create updated metadata for the repository on the repository
  server. This is done automatically if you ran `package/build` script
  via jenkins.
* run yum clean expire-cache on the target centos server. This will
  force yum to refetch updated repository contents next time you run
  yum search or install or upgrade.

# 4. install certificates into /etc/mantu

Grab the certificates from your local authorities and deploy it into /etc/mantu

```shell
[fisher@centos7 ~]$ tar ztf mantu.tgz
mantu/
mantu/MantuPush/
mantu/MantuPush/apns-b2b-certificate.pem
mantu/MantuPush/apns-b2b-key-noenc.pem
mantu/MantuPush/apns-cer.pem
mantu/MantuPush/apns-key-noenc.pem
mantu/MantuPush/apns-voip-cer.pem
mantu/MantuPush/apns-voip-key-noenc.pem
mantu/server.pem
[fisher@centos7 ~]$ sudo -s
# cd /etc
# tar zxf /home/fisher/mantu.tgz
```

# 5. ensure MySQL is installed and configured properly

check the installation:

```shell
[fisher@centos7 ~]$ rpm -qa |grep Maria
MariaDB-server-10.1.31-1.el7.centos.x86_64
MariaDB-client-10.1.31-1.el7.centos.x86_64
MariaDB-shared-10.1.31-1.el7.centos.x86_64
MariaDB-common-10.1.31-1.el7.centos.x86_64
```

...and if it's not installed,

```shell
# cat >/etc/yum.repos.d/MariaDB.repo
[mariadb]
name = MariaDB
baseurl = http://yum.mariadb.org/10.1/centos7-amd64
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB
gpgcheck=1
# yum install MariaDB-server MariaDB-client
# yum install postgresql-server
# systemctl enable mariadb
# systemctl start mariadb
# mysql -u root
```

and change the password for the user:

```sql
create database ejabberd;
create database history;
grant all on *.* to 'ejabberd'@'localhost' identified by 'xmpp11xmpp11';
```

Nota bene -- we need it **ALL** in order to CREATE DATABASE.

# 6. install apropriate version or just the latest one

```shell
$ sudo yum install mantu-ejabberd16-rel
```

Nota bene: we agreed to use -rel flavour of the package, because it
places everything in one place, and it seems easier to support.

Hint – if you want to install specific version of the package

```shell
$ sudo yum install mantu-ejabberd16-devel-rel-0.30.0.132
```

the pattern is `yum install packagename-version`

# 7. ensure the server is configured properly

the main configuration file is /opt/ejabberd/etc/ejabberd/ejabberd.yml

when upgrading package, the old configuration file should remain in
place, but for new installations you probably need to tweak it. Make
sure node_name option contains correct hostname, too.

# 8. start the ejabberd server

after installing the server does not start automatically. To start it,
please use `ejabberdctl` script,

```shell
$ sudo /opt/ejabberd/bin/ejabberdctl start
```

# 9. ensure it is working:

```shell
[fisher@centos7 ~]$ sudo /opt/ejabberd/bin/ejabberdctl status
[sudo] password for fisher:
The node ejabberd@localhost is started with status: started
node uptime is 0 days, 0 hours, 0 minutes and 11 seconds
ejabberd 0.27.0.15 is running in that node
```

# 10. grab some beverage and celebrate. It's done.

hurray!
