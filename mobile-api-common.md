# Common definitions for client API

## Security considerations 

Authentication for XMPP and HTTPS API is token-based. Client receives token during inivitation process, controlled by Dashboard (or with special HTTP poll handler if it is web client).

Token is a base64-encoded string treated as opaque value by client, internally it is a signed JWT token.
Every generated token has an RSA key pair (i.e. private and public keys), assigned to it by server.
Client receives public key with token in order to use it for encryption.

In addition to TLS, both XMPP and HTTPS APIs implements application-level AES-256 encryption. 
Client provides a randomly generated key and initialization vector for encryption, encrypted with public key, assigned to current authentication token. Server can then decrypt it using a private key stored in a persistent storage.
Then both parties can encrypt / decrypt application data with shared key / ivec using AES-256 in CBC mode.

## Chat message format

Chat messages passed via server or peer-to-peer connection has the same json-based format (from server's perspective):

```json
{
	"target": {
		"chat_type": 0 | 1,
		"to": <string>,
		"branch": <integer>},

	"payload": {
		"message": {
			"settings": {
				"options": {
					"copy_message": <boolean>,
					"forward_message": <boolean>,
					"privacy_type": 0 | 1
				},
				"self_desctruct": 
					{"timer_time" : <integer>}			 
			},

			"msg": {
				"id": <string>,
				"ts": <integer>,
				"type": <integer>
			},

			"parts": [<part>]
	} 

```
where `<part>` is an actual message content, whose possible format is determined by `"type"` value.
Possible type and part format combinations enumerated below.
text message:
```json
{"type": 2002,
	"parts": [
		{"type": 0, "data": <string>}
	]
} 
```

location message:
```json
{"type": 2018,
	"parts": [
		{"type": 3, "latitude": <float>, "longtitude": <float>}
	]
} 
```

purge event:
```json
{"type": 3100,
	"parts": [
		{"type": 1001}
	]
} 
```

message delete event:
```json
{"type": 3100,
	"parts": [
		{"type": 1002, "messageIds": [<string>]}
	]
} 
```

screenshot taken event:
```json
{"type": 3100,
	"parts": [
		{"type": 1003}
	]
} 
```

media message metadata delivery acknowledgement:
```json
{"type": 3100,
	"parts": [
		{"type": 400, "messageIds": [<string>]}
	]
} 
```

message delivery acknowledgement:
```json
{"type": 3100,
	"parts": [
		{"type": 401, "messageIds": [<string>]}
	]
} 
```

message read status acknowledgement:
```json
{"type": 3100,
	"parts": [
		{"type": 1004, "messageIds": [<string>]}
	]
} 
```

media message:
```json
{"type": 2007 | 2008 | 2009 | 2010,
	"parts": [
		{
			"type": 1 | 2,
			"id": <string>,
			"info": {
				"size": <integer>,
				"name": <string>,
				"duration": <integer>
			}
		}
	]
} 
```
where part type equals 1 for regular parts and 2 for thumbnails, message type determines media type:
2007 - audio,
2008 - video,
2009 - image,
2010 - document.

new group chat event:
```json
{"type": 2001,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>,
					"executor": <jid>,
					"name": <string>,
					"members": [<jid>],
					"img_id": <string>
				}
		}
	]
} 
```

new group chat event or notification to new member about group chat:
```json
{"type": 2001 | 2016,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>,
					"executor": <jid>,
					"name": <string>,
					"members": [<jid>],
					"img_id": <string>
				}
		}
	]
} 
```

group chat subject updated event:
```json
{"type": 2003,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>,
					"executor": <jid>,
					"name": <string>,
				}
		}
	]
} 
```

group chat image updated event:
```json
{"type": 2011,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>,
					"executor": <jid>,
					"img_id": <string>,
				}
		}
	]
} 
```

group chat delete event:
```json
{"type": 2011,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>, "executor": <jid>}
		}
	]
} 
```

group chat added (2016) or deleted (2013) members event:
```json
{"type": 2016 | 2013,
	"parts": [
		{
			"type": 300,
			"data":
				{"id": <string>, "executor": <jid>, "members": [<jid>]}
		}
	]
} 
```
